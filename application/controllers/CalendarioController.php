<?php

class CalendarioController extends Zend_Rest_Controller {
	
	public function init()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
	}
	
	public function getAction() {
		
	}
	
	public function indexAction() {
		echo '{"success":true,"message":"Loaded data","data":[{"id":1001,"cid":1,"start":"2012-03-06T10:00:00-03:00","end":"2012-03-16T15:00:00-03:00","title":"Vacation","notes":"Have fun", "custom":"BLABLABLA"},{"id":1002,"cid":2,"start":"2012-03-26T11:30:00-03:00","end":"2012-03-26T13:00:00-03:00","title":"Lunch with Matt","loc":"Chuys","url":"http:\/\/chuys.com","notes":"Order the queso"},{"id":1003,"cid":3,"start":"2012-03-26T15:00:00-03:00","end":"2012-03-26T15:00:00-03:00","title":"Project due"},{"id":1004,"cid":1,"start":"2012-03-26T00:00:00-03:00","end":"2012-03-26T00:00:00-03:00","title":"Sarahs birthday","ad":true,"notes":"Need to get a gift", "custom":"BLABLABLA NIVER"},{"id":1005,"cid":2,"start":"2012-03-14T00:00:00-03:00","end":"2012-04-04T23:59:59-03:00","title":"A long one...","ad":true},{"id":1006,"cid":3,"start":"2012-03-31T00:00:00-03:00","end":"2012-04-01T23:59:59-03:00","title":"School holiday"},{"id":1007,"cid":1,"start":"2012-03-26T09:00:00-03:00","end":"2012-03-26T09:30:00-03:00","title":"Haircut","notes":"Get cash on the way","rem":60},{"id":1008,"cid":3,"start":"2012-02-25T00:00:00-02:00","end":"2012-02-27T00:00:00-03:00","title":"An old event","ad":true},{"id":1009,"cid":2,"start":"2012-03-24T13:00:00-03:00","end":"2012-03-24T18:00:00-03:00","title":"Board meeting","loc":"ABC Inc.","rem":60},{"id":1010,"cid":3,"start":"2012-03-24T00:00:00-03:00","end":"2012-03-28T23:59:59-03:00","title":"Jennys final exams","ad":true},{"id":1011,"cid":1,"start":"2012-03-28T19:00:00-03:00","end":"2012-03-28T23:00:00-03:00","title":"Movie night","note":"Dont forget the tickets!","rem":60}]}';
	}
	
	public function postAction() {
		
	}
	
	public function putAction() {
		
	}
	
	public function deleteAction() {
		
	}
}