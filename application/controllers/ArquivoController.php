<?php

class ArquivoController extends JLib_Controller_StandardController {

	public function postAction() {
		$fileAdapter = new Zend_File_Transfer_Adapter_Http();
		$fileAdapter->setDestination('uploads');
		// Informações referente ao arquivo
		$fileAdapter->receive();
		$file = $fileAdapter->getFileInfo();

		try
		{
			$dataParams = $this->getRequest()->getPost();
			$dataParams['path'] = $fileAdapter->getDestination() . PATH_SEPARATOR . $file['arquivo']['name'];
			$dataParams['arquivo'] = $file['arquivo']['name'];
			
			$appModel = $this->initAppModel();
			
			if (!method_exists($appModel, 'crupRegister')):
				throw new Exception('ERROR: Method not found.');
			endif;
			
			$data = $appModel->crupRegister($dataParams);
			
			if (!(boolean) $data):
				throw new Exception('Falha detectada, nenhum registro foi criado.');
			endif;
			
			$return = array(
				'success' => 'true',
				'msg' => 'Registro incluido com sucesso.'
			);
		}
		catch (Helpers_Exception $exc)
		{
			$return = array(
				'success' => false,
				'msg' => $exc->getPublicMessage()
			);
		}
		catch (Exception $exc)
		{
			$return = array(
				'success' => false,
				'msg' => 'Desculpe ocorreu uma falha ao executar o sistema.'
			);
			//FB::log($exc->getMessage());
		}
		
		if ($return['success']) {
			try {
				if (!$fileAdapter->isValid($file)) {
					$return = array(
						'success' => false, 
						'msg' => 'Tipo de arquivo invalido'
					);
				}

				if (!$fileAdapter->isUploaded($file)) {
					$return = array(
						'success' => false, 
						'msg' => 'Você esqueceu de informar o arquivo.'
					);
				}

				//Envia para o servidor
				$fileAdapter->receive();

				// Obtem o nome do arquivo
	//			$names = $fileAdapter->getFileName('uploadedfile');

				// Retorna o tamanho do arquivo
	//			$size = $fileAdapter->getFileSize('uploadedfile');

				// Retorna o mimetype do arquivo (Se é jpeg, gif, pdf, etc)
	//			$type = $fileAdapter->getMimeType('uploadedfile');

				$return = array(
					'success' => true, 
					'msg' => 'Arquivo enviado.'
				);
			}
			catch (Zend_File_Transfer_Exception $exc) {
				$return = array(
					'success' => false,
					'msg' => $exc->getMessage()
				);
			}
		}
		
		echo Zend_Json::encode($return);
	}
}