<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

	public function indexAction() {
			//$userSession = new Zend_Session_Namespace('userSession');
//			if (!isset($userSession->userData)) {
//					$this->_redirect("/auth");
//			}

			//$file = file('../.svn/entries');

			$this->view->systemVersion = "Versão: 0.1";
			$this->view->pageTitle = "DeskJurídico - Painel do Usuário";
			$this->view->headScript()->appendFile('extjs/mainApp.js');
			$this->_helper->layout->enableLayout();
	}
}

