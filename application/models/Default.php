<?php

/**
 * DescriÃƒÆ’Ã‚Â§ÃƒÆ’Ã‚Â£o da classe DefaultModel
 *
 * @copyright Service Desk (www.srvdesk.com.br)
 * @author CÃƒÂ©sar JÃƒÂºnior (Analista de Sistemas/Engenheiro de Software)
 * @since 2011
 */
class App_Model_Default extends Zend_Db_Table {

    protected $_columnsAlias = null;
    protected $_resultQueryRows = null;
    protected $_totalQueryRows = null;
    protected $_userSession = null;
    protected $_systemMessage = null;

//	public function init() {
//		$userSession = new Zend_Session_Namespace('userSession');
//		$this->_userSession = $userSession->userData[0];
//	}
    /**
     *
     * @param array $options
     * @return App_Model_Default
     */
    public function setOptions(array $options) {

        parent::setOptions($options);

        foreach ($options as $chave => $valor) {
            switch ($chave) {
                case "columnsAlias":
                    $this->_columnsAlias = $valor;
                    break;
                default:
                    //Caso Ignorado...
                    break;
            }
        }
        return $this;
    }

    public function fetchRegisterDefault($where = null, $filter = null, $limitStart = null, $orderBy = null, $columns = array('*'), $returnArray = true) {
        $return = false;
        $select = $this->select();

        if (!empty($this->_columnsAlias)):
            $select->from(array($this->_columnsAlias => $this->_name), $columns, $this->_schema);
        else:
            $select->from(array($this->_name), $columns, $this->_schema);
        endif;

        if (!empty($where)):
            foreach ($where as $chave => $valor) {
                if (is_string($chave)):
                    $select->where($chave, $valor);
                else:
                    $select->where($valor);
                endif;
            }
        endif;

        if (!empty($filter)):
            foreach ($filter as $valor) {
                $select->where('LOWER(' . $valor['field'] . ') LIKE ?', strtolower($valor['data']['value']) . '%');
            }
        endif;

        $resultadoTotal = $this->fetchAll($select)->count();

        if (!empty($orderBy)):
            foreach ($orderBy as $valor) {
                $select->order($valor['property'] . " " . $valor['direction']);
            }
        endif;

        if (!empty($limitStart)):
            $select->limit($limitStart['limit'], $limitStart['start']);
        endif;
        
        //---------------------------------------------------------------------------------------------------------
        $resultado = $this->fetchAll($select);
        $resultadoCount = $resultado->count();

        $this->setResultQueryRows($resultadoCount);
        $this->setTotalQueryRows($resultadoTotal);

        if (!empty($resultadoCount)):
            if ($returnArray):
                $return = $resultado->toArray();
            else:
                $return = $resultado;
            endif;
        endif;

        return $return;
    }

    public function crupRegisterDefault($arrayParams = array(), $fieldRequired = array(), $fieldRegistered = array()) {

        $return = false;
        if (empty($arrayParams)):
            throw new Exception('ParÃ¢metros Insuficientes.');
        endif;

        if (empty($this->_primary)):
            throw new Exception('Primary Key not Found.');
        endif;

        $primaryKey = $this->_primary;
        $arrayParams = array_filter($arrayParams, function($valueArray) use ($arrayParams, $primaryKey) {
                    $return = true;
                    if ((key($arrayParams) == $primaryKey) && empty($valueArray)):
                        $return = false;
                    elseif ($valueArray == "" || is_null($valueArray)):
                        $return = false;
                    endif;
                    next($arrayParams);
                    return $return;
                });

        if (!empty($fieldRequired)):
            foreach ($fieldRequired as $nomeCampo => $msgException) {
                // Verifica se os campos obrigatÃƒÂ³rios definidos em $fieldRequired estÃƒÂ£o presentes no array gerado!!
                if (!array_key_exists($nomeCampo, $arrayParams)):
                    throw new Helpers_Exception($msgException);
                endif;
            }
        endif;

        if (!empty($fieldRegistered)):
            foreach ($fieldRegistered as $nomeCampo => $msgException) {
                if (array_key_exists($nomeCampo, $arrayParams)):
                    if (array_key_exists($this->_primary, $arrayParams)):
                        $where = array(
                            $nomeCampo . ' = ?' => $arrayParams[$nomeCampo],
                            $this->_primary . ' <> ?' => $arrayParams[$this->_primary]
                        );
                    else:
                        $where = array(
                            $nomeCampo . ' = ?' => $arrayParams[$nomeCampo]
                        );
                    endif;
                    $fetchReturn = $this->fetchRegisterDefault($where);
                    if (!empty($fetchReturn)):
                        throw new Helpers_Exception($msgException);
                    endif;
                endif;
            }
        endif;

//        $userSession = new Zend_Session_Namespace('userSession');
//        $idUsuario = $userSession->userData['id'];
//        $usuarioReferencia = $userSession->userData['usuarios_id_referencia'];

        if (!array_key_exists($this->_primary, $arrayParams)):
//            $arrayParams['fk_usuarios_id'] = $idUsuario;
//            $arrayParams['fk_usuarios_id_cad'] = $idUsuario;
//            $arrayParams['fk_usuarios_id_alt'] = $idUsuario;
//            $arrayParams['fk_usuarios_id_referencia'] = $usuarioReferencia;
            $return = $this->insert($arrayParams);
        else:
            $where = $this->getAdapter()->quoteInto($this->_primary . ' = ?', $arrayParams[$this->_primary]);
            $result = $this->fetchRow($where);
            if (!empty($result)):
//                $arrayParams['fk_usuarios_id_alt'] = $idUsuario;
//                $arrayParams['datahora_alt'] = new Zend_Db_Expr('NOW()');
                $return = $this->update($arrayParams, $where);
            else:
                throw new Helpers_Exception('O registro nÃ£o foi atualizado, possivel falha de indexaÃ§Ã£o.');
            endif;
        endif;
        return $return;
    }

    public function destroyRegisterDefault($idRegister = null, $arrayUpdate = array()) {
        $return = false;
        if (empty($idRegister)):
            throw new Exception('Parâmetros Insuficientes para remover registro.');
        endif;

        if (empty($this->_primary)):
            throw new Exception('Primary Key not Found.');
        endif;

        $where = $this->getAdapter()->quoteInto($this->_primary . ' = ?', $idRegister);
        $return = $this->fetchAll($where);
        $countReturn = $return->count();

        if ($countReturn != 1):
            throw new Helpers_Exception('Não foi possivel remover registro, possivel problema de indexação.');
        endif;

        $this->update($arrayUpdate, $where);
        return $return;
    }

    public function setResultQueryRows($resultQueryRows) {
        $this->_resultQueryRows = $resultQueryRows;
    }

    public function getResultQueryRows() {
        return $this->_resultQueryRows;
    }

    public function setTotalQueryRows($totalQueryRows) {
        $this->_totalQueryRows = $totalQueryRows;
    }

    public function getTotalQueryRows() {
        return $this->_totalQueryRows;
    }

    public function setSystemMessage($msgSistema = null) {
        $this->_systemMessage = $msgSistema;
    }

    public function getSystemMessage() {
        return $this->_systemMessage;
    }

}