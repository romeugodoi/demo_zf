<?php

class App_Model_Arquivo extends App_Model_Default
{

	protected $_name = 'arquivos';
	protected $_schema = 'dados';
	protected $_sequence = 'dados.arquivos_id_seq';
	protected $_primary = 'id';
	protected $_columnsAlias = 'a';

	public function fetchRegister($getParams = array(), $limitStart = null, $orderBy = null)
	{
		$return = false;

		$filter = (!empty($getParams['filter'])) ? $getParams['filter'] : null;

		$columns = array(
			'a.id',
			'a.nome',
			'a.descricao',
			'a.path',
			'a.arquivo'
		);
		$where = array();
		
		$data = $this->fetchRegisterDefault($where, $filter, $limitStart, $orderBy, $columns);
		
		if ((Boolean) $data):
//			foreach ($data as $chave => $valor)
//			{
//				$dateTime = new DateTime($valor['datahora_cad']);
//				$data[$chave]['datahora_cad'] = $dateTime->getTimestamp();
//			}
			$return = $data;
		endif;

		return $return;
	}
	
    public function crupRegister($getParams = array()) {
		$return = false;
        
        if (empty($getParams)):
            throw new Exception('Parâmentros Insuficientes.');
        endif;
	
        $fieldRequired = array(
			'nome' => 'Necessário preencher o campo nome.',
			'arquivo' => 'Necessário informar o arquivo para upload.'
        );
        
        $return = $this->crupRegisterDefault($getParams, $fieldRequired);
        
		return $return;
    }
}