Ext.define('Siccad.model.Arquivo', {
	extend: 'Ext.data.Model', 
	
	idProperty: "id",
     proxy: {
        type: 'rest',
        url : '/arquivo',
        noCache: false,
        reader: {
            type: 'json',
            idProperty: 'id',
            messageProperty: 'msg',
            root: 'data'
        }
     },   
	fields: [
		{
			name: 'id'
		}, 
		{
			name: 'nome'
		}, 
		{
			name: 'descricao'
		}, 
		{
			name: 'path'
		},
		{
			name: 'arquivo'
		}
	]
});