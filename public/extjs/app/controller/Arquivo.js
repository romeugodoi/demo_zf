Ext.require('Siccad.controller.AbstractController');

Ext.define('Siccad.controller.Arquivo', {
	extend: 'Siccad.controller.AbstractController',
    stores: ['Arquivos'],
	storePrincipal: 'Arquivos',
    models: ['Arquivo'], 
    editTitle: 'Edição de Arquivo',
    insertTitle: 'Cadastro de Arquivo',
    modelClass: 'Siccad.model.Arquivo',
	editWidget: 'arquivoEdit',
    gridWidget: 'arquivoList',
    
    views: [
		'arquivo.List',
		'arquivo.Edit'
    ],
    
    refs: [
		{
			ref: 'formEdit', 
			selector: 'arquivoEdit'
		},
		{
			ref: 'gridList', 
			selector: 'arquivoList'
		}
    ],

    init: function() {
        this.control({
            'arquivoList': {
                itemdblclick: this.edit
            },

            'arquivoList button[action=insert]': {
                click: this.insert
            },
            
            'arquivoList button[action=edit]': {
                click: this.edit
            },

            'arquivoList button[action=destroy]': {
                click: this.destroy
            },
            
            'arquivoList button[action=refresh]': {
                click: this.refresh
            },

            'arquivoEdit button[action=save]': {
                click: this.save
            }
        });
    },
    
    save: function(button, evt, opt){
        
        var win = button.up('window'),
            form = win.down('form').getForm(),
            id = form.getRecord() ? form.getRecord().get('id') : 0,
			store = this.getStore(this.storePrincipal);
		
        if (form.isValid()) {
			form.submit({
				url: '/arquivo',
				waitMsg: 'Uploading your file...',
				success: function(fp, o) {
					Ext.Msg.alert('Success', 'Your file has been uploaded.');
				}
			}); /*
            var record = form.getRecord(),
                values = form.getValues();
                
            if (record) {
                if(record.data['id']){
                    record.set(values);
                }
            } else {
                record = Ext.create(this.modelClass);
                record.set(values);
				store.add(record);
            }
            
            win.close();
            store.sync();*/
        } else {
            Ext.ux.Msg.flash({
                msg: 'Há campos preenchidos incorretamente',
                type: 'error'
            });
        }
    }
});
