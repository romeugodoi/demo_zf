Ext.define('Siccad.store.Arquivos', {
    extend: 'Siccad.store.AbstractStore',
    model: 'Siccad.model.Arquivo',
    autoLoad: false,
    remoteSort: true,
    pageSize: 10,
    sorters: [{
        property: 'nome',
        direction: 'ASC'
    }]
//    proxy: {
//        simpleSortMode: true,
//        type: 'ajax',
//        api: {
//            read: '/arquivo/fetchAll?json=true',
//            create: '/arquivo/create?json=true',
//            update: '/arquivo/edit?json=true',
//            destroy: '/arquivo/delete?json=true'
//        },
//        actionMethods: {
//            read: 'POST',
//            create: 'POST',
//            update: 'POST',
//            destroy: 'POST'
//        },  
//        reader: {
//            type: 'json',
//            root: 'data',
//            successProperty: 'success'
//        },
//        writer: {
//            type: 'json',
//            writeAllFields: true,
//            encode: true,
//            root: 'data'
//        },
//        listeners: {
//            exception: function(proxy, response, operation){
//                Ext.MessageBox.show({
//                    title: 'REMOTE EXCEPTION',
//                    msg: operation.getError(),
//                    icon: Ext.MessageBox.ERROR,
//                    buttons: Ext.Msg.OK
//                });
//            }
//        }
//    }
});