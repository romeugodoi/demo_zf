Extensible.calendar.data.EventMappings = {
    EventId: {
        name:    'EventId',
        mapping: 'id',
        type:    'int'
    },
    CalendarId: {
        name:    'CalendarId',
        mapping: 'cid',
        type:    'int'
    },
    Title: {
        name:    'Title',
        mapping: 'title',
        type:    'string'
    },
    StartDate: {
        name:       'StartDate',
        mapping:    'start',
        type:       'date',
        dateFormat: 'c'
    },
    EndDate: {
        name:       'EndDate',
        mapping:    'end',
        type:       'date',
        dateFormat: 'c'
    },
    RRule: { // not currently used
        name:    'RecurRule', 
        mapping: 'rrule', 
        type:    'string' 
    },
    Location: {
        name:    'Location',
        mapping: 'loc',
        type:    'string'
    },
    Notes: {
        name:    'Notes',
        mapping: 'notes',
        type:    'string'
    },
    Url: {
        name:    'Url',
        mapping: 'url',
        type:    'string'
    },
    IsAllDay: {
        name:    'IsAllDay',
        mapping: 'ad',
        type:    'boolean'
    },
    Reminder: {
        name:    'Reminder',
        mapping: 'rem',
        type:    'string'
    },
    Custom: {
        name:    'Custom',
        mapping: 'custom',
        type:    'string'
    }
};

Ext.override(Extensible.calendar.form.EventDetails, {
    extend: 'Ext.form.Panel',
    alias: 'widget.extensible.eventeditform',
    
    requires: [
        'Extensible.form.field.DateRange',
        'Extensible.calendar.form.field.ReminderCombo',
        'Extensible.calendar.data.EventMappings',
        'Extensible.calendar.form.field.CalendarCombo',
        'Extensible.form.recurrence.Combo',
        'Ext.layout.container.Column'
    ],
    
    labelWidth: 65,
    labelWidthRightCol: 65,
    colWidthLeft: .6,
    colWidthRight: .4,
    title: 'Event Form',
    titleTextAdd: 'Add Event',
    titleTextEdit: 'Edit Event',
    titleLabelText: 'Title',
    datesLabelText: 'When',
    reminderLabelText: 'Reminder',
    notesLabelText: 'Notes',
    locationLabelText: 'Location',
    webLinkLabelText: 'Web Link',
    calendarLabelText: 'Calendar',
    repeatsLabelText: 'Repeats',
    
    customLabelText: 'Custom', // Add Custom
    
    saveButtonText: 'Save',
    deleteButtonText: 'Delete',
    cancelButtonText: 'Cancel',
    bodyStyle: 'padding:20px 20px 10px;',
    border: false,
    buttonAlign: 'center',
    autoHeight: true, // to allow for the notes field to autogrow
    
    /* // not currently supported
     * @cfg {Boolean} enableRecurrence
     * True to show the recurrence field, false to hide it (default). Note that recurrence requires
     * something on the server-side that can parse the iCal RRULE format in order to generate the
     * instances of recurring events to display on the calendar, so this field should only be enabled
     * if the server supports it.
     */
    enableRecurrence: false,
    
    // private properties:
    layout: 'column',
    
    // private
    initComponent: function(){
        
        this.addEvents({
            /**
             * @event eventadd
             * Fires after a new event is added
             * @param {Extensible.calendar.form.EventDetails} this
             * @param {Extensible.calendar.data.EventModel} rec The new {@link Extensible.calendar.data.EventModel record} that was added
             */
            eventadd: true,
            /**
             * @event eventupdate
             * Fires after an existing event is updated
             * @param {Extensible.calendar.form.EventDetails} this
             * @param {Extensible.calendar.data.EventModel} rec The new {@link Extensible.calendar.data.EventModel record} that was updated
             */
            eventupdate: true,
            /**
             * @event eventdelete
             * Fires after an event is deleted
             * @param {Extensible.calendar.form.EventDetails} this
             * @param {Extensible.calendar.data.EventModel} rec The new {@link Extensible.calendar.data.EventModel record} that was deleted
             */
            eventdelete: true,
            /**
             * @event eventcancel
             * Fires after an event add/edit operation is canceled by the user and no store update took place
             * @param {Extensible.calendar.form.EventDetails} this
             * @param {Extensible.calendar.data.EventModel} rec The new {@link Extensible.calendar.data.EventModel record} that was canceled
             */
            eventcancel: true
        });
                
        this.titleField = Ext.create('Ext.form.TextField', {
            fieldLabel: this.titleLabelText,
            name: Extensible.calendar.data.EventMappings.Title.name,
            anchor: '90%'
        });
        this.dateRangeField = Ext.create('Extensible.form.field.DateRange', {
            fieldLabel: this.datesLabelText,
            singleLine: false,
            anchor: '90%',
            listeners: {
                'change': Ext.bind(this.onDateChange, this)
            }
        });
        this.reminderField = Ext.create('Extensible.calendar.form.field.ReminderCombo', {
            name: Extensible.calendar.data.EventMappings.Reminder.name,
            fieldLabel: this.reminderLabelText,
            anchor: '70%'
        });
        this.notesField = Ext.create('Ext.form.TextArea', {
            fieldLabel: this.notesLabelText,
            name: Extensible.calendar.data.EventMappings.Notes.name,
            grow: true,
            growMax: 150,
            anchor: '100%'
        });
        this.locationField = Ext.create('Ext.form.TextField', {
            fieldLabel: this.locationLabelText,
            name: Extensible.calendar.data.EventMappings.Location.name,
            anchor: '100%'
        });
        this.urlField = Ext.create('Ext.form.TextField', {
            fieldLabel: this.webLinkLabelText,
            name: Extensible.calendar.data.EventMappings.Url.name,
            anchor: '100%'
        });
        
        // CUSTOM FIELD
        this.customField = Ext.create('Ext.form.TextField', {
            fieldLabel: this.customLabelText,
            name: Extensible.calendar.data.EventMappings.Custom.name,
            anchor: '100%'
        });
        
        var leftFields = [this.titleField, this.dateRangeField, this.reminderField, this.customField], 
            rightFields = [this.notesField, this.locationField, this.urlField];
            
        if(this.enableRecurrence){
            this.recurrenceField = Ext.create('Extensible.form.recurrence.Fieldset', {
                name: Extensible.calendar.data.EventMappings.RRule.name,
                fieldLabel: this.repeatsLabelText,
                anchor: '90%'
            });
            leftFields.splice(2, 0, this.recurrenceField);
        }
        
        if(this.calendarStore){
            this.calendarField = Ext.create('Extensible.calendar.form.field.CalendarCombo', {
                store: this.calendarStore,
                fieldLabel: this.calendarLabelText,
                name: Extensible.calendar.data.EventMappings.CalendarId.name,
                anchor: '70%'
            });
            leftFields.splice(2, 0, this.calendarField);
        };
        
        this.items = [{
            id: this.id+'-left-col',
            columnWidth: this.colWidthLeft,
            layout: 'anchor',
            fieldDefaults: {
                labelWidth: this.labelWidth
            },
            border: false,
            items: leftFields
        },{
            id: this.id+'-right-col',
            columnWidth: this.colWidthRight,
            layout: 'anchor',
            fieldDefaults: {
                labelWidth: this.labelWidthRightCol || this.labelWidth
            },
            border: false,
            items: rightFields
        }];
        
        this.fbar = [{
            text:this.saveButtonText, scope: this, handler: this.onSave
        },{
            itemId:this.id+'-del-btn', text:this.deleteButtonText, scope:this, handler:this.onDelete
        },{
            text:this.cancelButtonText, scope: this, handler: this.onCancel
        }];
        
        this.addCls('ext-evt-edit-form');
        
        this.callParent(arguments);
    },
    
    // private
    onDateChange: function(dateRangeField, val){
        if(this.recurrenceField){
            this.recurrenceField.setStartDate(val[0]);
        }
    },
    
    // inherited docs
    loadRecord: function(rec){
        this.form.reset().loadRecord.apply(this.form, arguments);
        this.activeRecord = rec;
        this.dateRangeField.setValue(rec.data);
        
        if(this.recurrenceField){
            this.recurrenceField.setStartDate(rec.data[Extensible.calendar.data.EventMappings.StartDate.name]);
        }
        if(this.calendarStore){
            this.form.setValues({'calendar': rec.data[Extensible.calendar.data.EventMappings.CalendarId.name]});
        }
        
        //this.isAdd = !!rec.data[Extensible.calendar.data.EventMappings.IsNew.name];
        if(rec.phantom){
            this.setTitle(this.titleTextAdd);
            this.down('#' + this.id + '-del-btn').hide();
        }
        else {
            this.setTitle(this.titleTextEdit);
            this.down('#' + this.id + '-del-btn').show();
        }
        this.titleField.focus();
    },
    
    // inherited docs
    updateRecord: function(){
        var dates = this.dateRangeField.getValue(),
            M = Extensible.calendar.data.EventMappings,
            rec = this.activeRecord,
            fs = rec.fields,
            dirty = false;
            
        rec.beginEdit();
        
        //TODO: This block is copied directly from BasicForm.updateRecord.
        // Unfortunately since that method internally calls begin/endEdit all
        // updates happen and the record dirty status is reset internally to
        // that call. We need the dirty status, plus currently the DateRangeField
        // does not map directly to the record values, so for now we'll duplicate
        // the setter logic here (we need to be able to pick up any custom-added 
        // fields generically). Need to revisit this later and come up with a better solution.
        fs.each(function(f){
            var field = this.form.findField(f.name);
            if(field){
                var value = field.getValue();
                if (value.getGroupValue) {
                    value = value.getGroupValue();
                } 
                else if (field.eachItem) {
                    value = [];
                    field.eachItem(function(item){
                        value.push(item.getValue());
                    });
                }
                rec.set(f.name, value);
            }
        }, this);
        
        rec.set(M.StartDate.name, dates[0]);
        rec.set(M.EndDate.name, dates[1]);
        rec.set(M.IsAllDay.name, dates[2]);
        
        dirty = rec.dirty;
        //delete rec.store; // make sure the record does not try to autosave
        rec.endEdit();
        
        return dirty;
    },
    
    // private
    onCancel: function(){
        this.cleanup(true);
        this.fireEvent('eventcancel', this, this.activeRecord);
    },
    
    // private
    cleanup: function(hide){
        if (this.activeRecord) {
            this.activeRecord.reject();
        }
        delete this.activeRecord;
        
        if (this.form.isDirty()) {
            this.form.reset();
        }
    },
    
    // private
    onSave: function(){
        if(!this.form.isValid()){
            return;
        }
        if(!this.updateRecord()){
            this.onCancel();
            return;
        }
        this.fireEvent(this.activeRecord.phantom ? 'eventadd' : 'eventupdate', this, this.activeRecord);
    },

    // private
    onDelete: function(){
        this.fireEvent('eventdelete', this, this.activeRecord);
    }
});

// Don't forget to reconfigure!
Extensible.calendar.data.EventModel.reconfigure();
Extensible.calendar.data.CalendarModel.reconfigure();

		//Override layout and items
//		Ext.override(Ext.ensible.cal.EventEditForm, {
//			layout: 'form',
//
//			initComponent: function() {
//				this.addEvents({
//					eventadd: true,
//					eventupdate: true,
//					eventdelete: true,
//					eventcancel: true
//				});
//
//				this.titleField = new Ext.form.TextField({
//					fieldLabel: this.titleLabelText,
//					name: Ext.ensible.cal.EventMappings.Title.name,
//					anchor: '95%'
//				});
//				this.dateRangeField = new Ext.ensible.cal.DateRangeField({
//					fieldLabel: this.datesLabelText,
//					singleLine: false,
//					anchor: '95%',
//					listeners: {
//						'change': this.onDateChange.createDelegate(this)
//					}
//				});
//				this.reminderField = new Ext.ensible.cal.ReminderField({
//					name: Ext.ensible.cal.EventMappings.Reminder.name,
//					fieldLabel: this.reminderLabelText
//				});
//				this.notesField = new Ext.form.HtmlEditor({
//					fieldLabel: this.notesLabelText,
//					name: Ext.ensible.cal.EventMappings.Notes.name,
//					grow: true,
//					growMax: 150,
//					anchor: '95%',
//					height: 150
//				});
//				this.locationField = new Ext.form.TextField({
//					fieldLabel: this.locationLabelText,
//					name: Ext.ensible.cal.EventMappings.Location.name,
//					anchor: '95%'
//				});
//				this.urlField = new Ext.form.TextField({
//					fieldLabel: this.webLinkLabelText,
//					name: Ext.ensible.cal.EventMappings.Url.name,
//					anchor: '95%'
//				});
//
//				this.recurrenceField = new Ext.ensible.cal.RecurrenceField({
//					name: Ext.ensible.cal.EventMappings.RRule.name,
//					fieldLabel: this.repeatsLabelText,
//					anchor: '95%'
//				});
//
//				this.calendarField = new Ext.ensible.cal.CalendarCombo({
//					store: this.calendarStore,
//					fieldLabel: this.calendarLabelText,
//					name: Ext.ensible.cal.EventMappings.CalendarId.name
//				});
//
//				this.items = [this.titleField, this.locationField, this.dateRangeField, this.calendarField, this.recurrenceField, this.reminderField, this.notesField, this.urlField];
//
//				this.fbar = [
//					{
//						text: this.saveButtonText, scope: this, handler: this.onSave
//					}, {
//						cls: 'ext-del-btn', text: this.deleteButtonText, scope: this, handler: this.onDelete
//					}, {
//						text: this.cancelButtonText, scope: this, handler: this.onCancel
//					}
//				];
//
//				Ext.ensible.cal.EventEditForm.superclass.initComponent.call(this);
//			},
//
//			onLayout: Ext.ensible.cal.EventEditForm.prototype.onLayout.createSequence(function() {
//				//This fix is not required in my application. But on this demonstration page, the form height is 0 if autoHeight is set to false.
//				//But that is required for attaching scrollbars in form layout.
//				this.el.child('form').dom.style.height = '';
//			})
//		});



var jApp = {
    showNotification: function(config) {        
        Ext.create('widget.uxNotification', {
            corner: 'br',
            manager: 'viewport',
            cls: config.cls,
            iconCls: config.icon,
            closable: true,
            resizable: false,
            title: config.title,
            html: config.html,
            slideInDelay: 800,
            slideDownDelay: 1500,
            slideInAnimation: 'elasticIn',
            slideDownAnimation: 'elasticIn'
        }).show();
    },
    createRestStore: function(model, store, sorters, extraParams) {
        if(store === null || store === undefined) {
            return false;
        }else {
            sorters = sorters || null;
            extraParams = extraParams || null;
            if(Ext.typeOf(model._setParams) == 'function') {
                model._setParams(extraParams);
            }
            return  Ext.create(store, {
                sorters: sorters,
                model: model._getMyName(),            
                proxy: model._getMyProxy(),
                pageSize: 25
            }); 
        }
    },
    //    fieldMask: function(obj, mask, fn) {        
    //        var plugin = new Ext.ux.Field.vType.InputTextMask(mask);             
    //        obj.plugins = [obj.initPlugin(plugin, false)]; 
    //        obj.on("keypress", function() {
    //            var val = parseInt(obj.getValue().replace("-", "").replace(".", "").replace("_", "")),
    //            maxLength = parseInt(mask.replace("-", "").replace(".", "").replace("_", "")),
    //            valCount = String(val).length,
    //            maskCount = String(maxLength).length;  
    //            if(valCount === maskCount){
    //                fn();
    //            } 
    //        })
    //    },
    fieldMask: function(obj, mask, fn) {
        fn = null || fn;
        $("#"+obj.getInputId()).mask(mask,{
            placeholder: " ",
            completed: fn
        });
    },
    currencyMask: function(obj) {
        $("#"+obj.getInputId()).maskMoney({
            symbol:'R$ ', 
            showSymbol:true, 
            thousands:'.', 
            decimal:',', 
            symbolStay: true
        });
    }
};

Ext.override(Ext.data.Model, {
    _getMyProxy: function() {
        return this.getProxy();
    },
    _getMyName: function() {
        return this.self.getName();
    },
    _setParams: function(params) {
        Ext.apply(this.getProxy(), {
            extraParams: params
        });
    }
});

Ext.override(Ext.form.Basic, {
    isValid: function() {
        var me = this,
        invalid;
        me.batchLayouts(function() {
            invalid = me.getFields().filterBy(function(field) {
                return !field.validate();
            });
        });
        var setActiveTabExecuted = false;
        me.getFields().each(function(field){
            if(!setActiveTabExecuted){
                var errors = field.getErrors();
                if (errors.length > 0){
                    var container = field.ownerCt;            
                    while (typeof container == 'object') {
                        if (Ext.getClassName(container.ownerCt) == 'Ext.tab.Panel'){
                            container.ownerCt.setActiveTab(container);
                            setActiveTabExecuted = true;
                        }
                        container = container.ownerCt;
                    }
                }
            }
        });
        return invalid.length < 1;
    }
});

Ext.apply(Ext.MessageBox, {
    updateButtonText: function(buttons){
        for (var i in buttons)
            this.buttonText[i] = buttons[i];

        this.msgButtons = [];
        for (i = 0; i < 4; i++) {
            button = this.makeButton(i);
            this.msgButtons[button.itemId] = button;
            this.msgButtons.push(button);
        }
        this.bottomTb = Ext.create('Ext.toolbar.Toolbar', {
            ui: 'footer',
            dock: 'bottom',
            layout: {
                pack: 'center'
            },
            items: [
            this.msgButtons[0],
            this.msgButtons[1],
            this.msgButtons[2],
            this.msgButtons[3]
            ]
        });
        this.removeDocked(this.dockedItems.items[0]);
        this.addDocked(this.bottomTb);
    }

});

//Renomeando texto de botoes da janela de alerta
Ext.MessageBox.updateButtonText({
    yes:'Sim',
    no: 'Não',
    cancel: 'Cancelar'
});

//Substituindo components Placeholder por um novo componente
Ext.override(Ext.AbstractComponent, {
    rplCmp: function(newCmp) {
        var oldCmp = this, 
        parent = oldCmp.up(),
        index;
        parent.items.each(function(comp,ind) {
            if (comp==oldCmp){
                index=ind;
            } 
        });
        parent.remove(oldCmp, true);
        newCmp = parent.insert(index, newCmp);
        parent.doLayout();
        return newCmp;
    }
});

//System configurations
Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = '/extjs/lib/resources/themes/images/default/tree/s.gif'; 
Ext.form.Panel.prototype.labelAlign = 'left';
Ext.form.ComboBox.prototype.minChars = 1;
Ext.Window.prototype.constrain = true;
Ext.Window.prototype.shadow = true;
Ext.tab.Panel.prototype.activeTab = 0;
Ext.form.Panel.prototype.iconCls = 'aba-icon';
Ext.form.field.Date.prototype.format = "d/m/Y";
Ext.form.field.Checkbox.prototype.inputValue = true;
Ext.form.field.Checkbox.prototype.uncheckedValue = false;
Ext.form.field.ComboBox.prototype.pageSize = 10;
Ext.form.field.ComboBox.prototype.listConfig = {
    minWidth: 270
};
//Ext.grid.Panel.prototype.listeners = {
//    render: function(obj) {
//        obj.setDisabled(true);
//        var myMask = new Ext.LoadMask(obj, {
//            msg:"Carregando informações..."
//        });
//        myMask.show();
//        obj.store.on({
//            load: function() {
//                myMask.destroy();
//                obj.setDisabled(false);
//            }
//        });
//    }
//}
//Ext.grid.column.Column.prototype.flex = 0;                
Ext.grid.View.prototype.loadMask = true;                
Ext.grid.BooleanColumn.prototype.falseText = '<font color="red"><b>Não</b></font>';
Ext.grid.BooleanColumn.prototype.trueText = '<font color="green"><b>Sim</b></font>';

Ext.getBody().on("contextmenu", Ext.emptyFn, null, {
    preventDefault: true
});

if (window.console && window.console.firebug) {
//    console = {
//        log: function() {}
//    };
}

//Definindo a classe override para ser importada no app
Ext.define('Ext.ux.override', {
    constructor: function() {}
});