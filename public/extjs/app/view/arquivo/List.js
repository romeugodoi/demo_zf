Ext.require(['Siccad.view.AbstractList', 'Ext.ux.grid.FiltersFeature']);

Ext.define('Siccad.view.arquivo.List', {
	extend: 'Siccad.view.AbstractList',	
	alias: 'widget.arquivoList', 
	modulo: 'Arquivo', 
	title: 'Lista de Arquivos', 
	selModel: Ext.create('Ext.selection.CheckboxModel'),
	columnLines: true,
    features: [{
        ftype: 'filters',
        // autoReload: false,
        // encode and local configuration options defined previously for easier reuse
        encode: false, // json encode the filter query
        local: true,   // defaults to false (remote filtering)

        // Filters are most naturally placed in the column definition, but can also be
        // added here.
        filters: [
            {
                type: 'boolean',
                dataIndex: 'visible'
            }
        ]
    }],
    
    /**
     * Custom function used for column renderer
     * @param {Object} value
     * @param {Object} p, 
     * @param {Object} record
     */
    renderLink:  function(value, p, record) {
        return Ext.String.format(
            '<a href="http://demo.zf.com.br/downloads/{1}" target="_blank">{3}</a>',
            value,
            record.data.path,
            record.getId(),
            record.data.arquivo
        );    	
    },
    
    initComponent: function(){
        
        
		this.columns = [
	        Ext.create('Ext.grid.RowNumberer'),
	        {header: 'ID',  dataIndex: 'id', filterable: true, filter: {type: 'numeric'}, width: 30},
	        {header: 'Nome',  dataIndex: 'nome', flex: 1, filter: true},
	        {header: 'Descrição',  dataIndex: 'descricao',  flex: 1, filter: true}, 
	        {header: 'Arquivo', dataIndex: 'arquivo', renderer: this.renderLink}
		];
	    
        this.callParent();
    }
});