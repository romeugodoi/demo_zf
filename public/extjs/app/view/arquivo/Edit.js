Ext.require(['Siccad.view.AbstractForm']);
Ext.require(['Siccad.view.AbstractWindow']);

Ext.define('Siccad.view.arquivo.Edit', {
    extend: 'Siccad.view.AbstractWindow',
    alias : 'widget.arquivoEdit',
    title : 'Edição de Arquivo',

    initComponent: function() {
    	
        this.items = [{
            xtype: 'abstractForm',
			frame:true,
			//renderTo: 'fi-form',
            items: [
            	{
	                xtype: 'textfield',
	                name : 'nome',
	                ref: 'nome',
	                fieldLabel: 'Nome',
	                allowBlank: false
            	},
            	{
	                xtype: 'textfield',
	                name : 'descricao',
	                ref: 'descricao',
	                fieldLabel: 'Descrição',
                    width: 50,
	                allowBlank: false
            	},
            	{
	                xtype: 'filefield',
	                name : 'arquivo',
	                ref: 'arquivo',
	                fieldLabel: 'Arquivo',
				emptyText: 'Select an image',
				buttonText: '',
				buttonConfig: {
					iconCls: 'upload-icon'
				}
            	}
            ]}
        ];

        this.callParent(arguments);
    }
});