Ext.require(['Extensible.calendar.CalendarPanel']);

Ext.define('Siccad.view.calendario.Calendario', {
    extend: 'Extensible.calendar.CalendarPanel',
    
    alias: 'widget.calendario', 
    
    eventStore: Ext.create('Siccad.store.Calendarios'),
    
    title: 'Basic Calendar'
//    width: 700,
//    height: 500

});