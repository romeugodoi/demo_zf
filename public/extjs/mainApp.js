(function() {
    Ext.Loader.setPath('Ext', 'extjs/lib/src');
    Ext.Loader.setPath('Ext.ux', 'extjs/app/ux');
	Ext.Loader.setPath("Extensible", "extjs/src");
    Ext.Loader.setConfig({
        enabled: true,
	    disableCaching: true 
//	    paths: {
//	        "Extensible": "extjs/src"
//	        "Extensible.example": "../.."
//    	}
    });

	Ext.require([
		'Ext.tree.*',
		'Ext.data.*',
		'Ext.tip.*'
		]);

	Ext.require('Ext.chart.*');
	Ext.require(['Ext.Window', 'Ext.layout.container.Fit', 'Ext.fx.target.Sprite', 'Ext.ux.grid.FiltersFeature']);
	
    Ext.application({
        name: 'Siccad',    
        appFolder: "extjs/app",
        controllers: [
			"Cidade",
			"Estado", 
			"Arquivo" 
//			"PessoasController",
//			"ProcessosController"
        ],
        requires: [         
			'Ext.ux.override', 
			'Siccad.view.calendario.Calendario'
//			'Ext.ux.japp.SelectButton',
//			'Ext.ux.japp.SelectWindow',
//			'Ext.ux.msg.Notification',
//			'Ext.ux.grid.FiltersFeature',
//			'Ext.ux.field.vType.InputTextMask'
        ],
		autoCreateViewport: true,
		launch: function() {
			Siccad.app = this;
		}
    });
})();
