<?php
/**
 * @author Romeu Godoi <romeu.godoi@gmail.com>
 * @todo
 *	Checar permissões por módulo para montagem dinâmica do menu
 */
$menu = "{ 
            children: [
                    {
                        text:'Geral',
                        expanded: true,
                        children:[
                            {
                                text:'Estados',
                                leaf: true,
                                xtypeClass: 'estadoList'
                            },
                            {
                                text:'Cidades',
                                leaf: true,
                                xtypeClass: 'cidadeList'
                            },
                            {
                                text:'Arquivos',
                                leaf: true,
                                xtypeClass: 'arquivoList'
                            },
                            {
                                text:'Calendário',
                                leaf: true,
                                xtypeClass: 'calendario'
                            }
                        ]
                    }
                ]
            }";
echo $menu;
