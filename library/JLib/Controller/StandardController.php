<?php

/**
 * DescriÃƒÂ§ÃƒÂ£o da classe StandardController
 *
 * @copyright Service Desk (www.srvdesk.com.br)
 * @author CÃƒÂ©sar JÃƒÂºnior (Analista de Sistemas/Engenheiro de Software)
 * @since 2011
 */
class JLib_Controller_StandardController extends Zend_Rest_Controller
{

	public function init()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		//$this->checkSession();
	}

	public function checkSession()
	{
		$getParams = $this->getRequest()->getParams();
		$paramsController = (!empty($getParams['controller'])) ? $getParams['controller'] : null;
		$userSession = new Zend_Session_Namespace('userSession');
		
		if (empty($userSession->userData) && $paramsController != "index" && $paramsController != "auth" && $paramsController != "session"):
			throw new Exception('Session not registered.');
		endif;
	}

	protected function initAppModel()
	{
		$accessController = $this->getRequest()->getControllerName();
		$className = "App_Model_" . ucfirst($accessController);

		if (!class_exists($className)):
			throw new Exception('ERROR: App not found (' . $className . ').');
		endif;
		
		return new $className();
	}

	public function indexAction()
	{
		try
		{
			$getParams = $this->getRequest()->getParams();
			$limit = (!empty($getParams['limit'])) ? $getParams['limit'] : null;
			$start = (!empty($getParams['start'])) ? $getParams['start'] : '0';
			
			$limitStart = (!empty($limit)) ? array(
				'limit' => $limit,
				'start' => $start) : null;
			
			$p = "\\"; 
			$orderBy  = (!empty($getParams['sort'])) ? Zend_Json::decode(str_replace($p, "", $getParams['sort'])) : null;
			$appModel = $this->initAppModel();
		
			if (!method_exists($appModel, 'fetchRegister')):
				throw new Exception('ERROR: Method not found.');
			endif;
			
			$data = $appModel->fetchRegister($getParams, $limitStart, $orderBy);
			
			if (!(boolean) $data):
				throw new Helpers_Exception('Nenhum resultado encontrado.');
			endif;
			
			$return = array(
				'success' => true,
				'results' => $appModel->getResultQueryRows(),
				'total' => $appModel->getTotalQueryRows(),
				'data' => $data
			);
		}
		catch (Helpers_Exception $exc)
		{
			$return = array(
				'success' => false,
				'msg' => $exc->getPublicMessage()
			);
		}
		catch (Exception $exc)
		{
			$return = array(
				'success' => false,
				'msg' => 'Desculpe ocorreu uma falha ao recuperar informações do sistema.' . print_r($exc->getTrace())
			);
			
			//FB::log($exc->getMessage());
		}
		echo Zend_Json::encode($return);
	}

	public function getAction()
	{
		throw new Exception('Acesso Negado');
	}

	public function postAction()
	{
		try
		{
			$getRawBody = $this->getRequest()->getRawBody();
			$dataParams = (!empty($getRawBody)) ? Zend_Json::decode($getRawBody) : null;
			$appModel = $this->initAppModel();
			
			if (!method_exists($appModel, 'crupRegister')):
				throw new Exception('ERROR: Method not found.');
			endif;
			
			$data = $appModel->crupRegister($dataParams);
			
			if (!(boolean) $data):
				throw new Exception('Falha detectada, nenhum registro foi criado.');
			endif;
			
			$return = array(
				'success' => 'true',
				'msg' => 'Registro incluido com sucesso.'
			);
		}
		catch (Helpers_Exception $exc)
		{
			$return = array(
				'success' => false,
				'msg' => $exc->getPublicMessage()
			);
		}
		catch (Exception $exc)
		{
			$return = array(
				'success' => false,
				'msg' => 'Desculpe ocorreu uma falha ao executar o sistema.'
			);
			//FB::log($exc->getMessage());
		}
		echo Zend_Json::encode($return);
	}

	public function putAction()
	{
		try
		{
			$getRawBody = $this->getRequest()->getRawBody();
			$dataParams = (!empty($getRawBody)) ? Zend_Json::decode($getRawBody) : null;
			$appModel = $this->initAppModel();
			if (!method_exists($appModel, 'crupRegister')):
				throw new Exception('ERROR: Method not found.');
			endif;
			$data = $appModel->crupRegister($dataParams);
			if (!(boolean) $data):
				throw new Exception('Falha detectada, nenhum registro foi modificado.');
			endif;
			$return = array(
				'success' => 'true',
				'msg' => 'Registro modificado com sucesso.'
			);
		}
		catch (Helpers_Exception $exc)
		{
			$return = array(
				'success' => false,
				'msg' => $exc->getPublicMessage()
			);
		}
		catch (Exception $exc)
		{
			$return = array(
				'success' => false,
				'msg' => 'Desculpe ocorreu uma falha ao executar o sistema.'
			);
			//FB::log($exc->getMessage());
		}
		echo Zend_Json::encode($return);
	}

	public function deleteAction()
	{
		try
		{
			$getParams = $this->getRequest()->getParams();
			$dataParams = (!empty($getParams['id'])) ? Zend_Json::decode($getParams['id']) : null;
			$appModel = $this->initAppModel();
			if (!method_exists($appModel, 'destroyRegister')):
				throw new Exception('ERROR: Method not found.');
			endif;
			$data = $appModel->destroyRegister($dataParams);
			if (!(boolean) $data):
				throw new Exception('Nenhum registro foi removido.');
			endif;
			$return = array(
				'success' => true,
				'msg' => $appModel->getSystemMessage()
			);
		}
		catch (Helpers_Exception $exc)
		{
			$return = array(
				'success' => false,
				'msg' => $exc->getPublicMessage()
			);
		}
		catch (Exception $exc)
		{
			$return = array(
				'success' => false,
				'msg' => 'Desculpe ocorreu uma falha ao executar o sistema.'
			);
			//FB::log($exc->getMessage());
		}
		echo Zend_Json::encode($return);
	}

}